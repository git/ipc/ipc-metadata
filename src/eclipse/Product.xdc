/*
 * Copyright (c) 2012-2014, Texas Instruments Incorporated
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

import xdc.tools.product.IProduct;

metaonly module Product inherits IProduct {

    override config String name = "IPC";

    override config String id   = "com.ti.rtsc.IPC";

    override config String version = "3.51.00.00";

    override config String companyName = "Texas Instruments Incorporated";

    override config IProduct.UrlDescriptor productDescriptor = {
        url: "http://processors.wiki.ti.com/index.php/Category:IPC",
        text: "IPC"
    };

    override config IProduct.UrlDescriptor licenseDescriptor = {
        text: "Released under the BSD license."
    };

    override config String copyRightNotice =
        "Copyright (c) 2010-2014 Texas Instruments Incorporated";

    override config String repositoryArr[] = ["packages"];

    override config String docsLocArr[] = [
        ".",
        "docs"
    ];

    override config MacroDescriptor macro = {
        name: "IPC_CG_ROOT",
        desc: "Returns the absolute path to the location of the version of IPC selected in Window->Preferences->CCS->RTSC preference page. If no IPC version is selected, the location of the highest available version is returned."
    };

    override config String productViewModule =
        "ti.sdo.ipc.productview.IpcProductView";

    override config String bundleName = "ipc";

    override config IProduct.HelpToc helpTocArr[] = [
        {
            label: "Release Notes",
            filePath: "ipc_3_51_00_00_release_notes.html",
            tocFile:false
        },
        {
            label: "Installation Guide (BIOS)",
            filePath: "IPC_Install_Guide_BIOS.pdf",
            tocFile:false
        },
        {
            label: "Users Guide",
            filePath: "IPC_Users_Guide.pdf",
            tocFile:false
        },
        {
            label: "API reference",
            filePath: "cdoc/eclipse_cdoc_toc.xml",
            tocFile:true
        },
    ];
}
