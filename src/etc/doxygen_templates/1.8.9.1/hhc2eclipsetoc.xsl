<?xml version="1.0" encoding="utf-8"?>
  <xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="xhtml"
	version="1.0">

  <xsl:output
	method="xml"
	indent="yes"/>

  <xsl:param name="title">FIXME</xsl:param>

  <xsl:template match="/">
	<xsl:apply-templates select="xhtml:html"/>
  </xsl:template>

  <xsl:template match="xhtml:html">
	<xsl:apply-templates select="xhtml:body/xhtml:ul"/>
  </xsl:template>

  <xsl:template match="xhtml:body/xhtml:ul">
	<toc label="{$title}">
	  <xsl:apply-templates select="xhtml:li"/>
	</toc>
  </xsl:template>

  <xsl:template match="xhtml:li">
	<topic>
	  <xsl:attribute name="href">html/<xsl:value-of select="xhtml:object/xhtml:param[@name = 'Local']/@value"/>
	  </xsl:attribute>
	  <xsl:attribute name="label">
		<xsl:value-of select="xhtml:object/xhtml:param[@name = 'Name']/@value"/>
	  </xsl:attribute>
	  <xsl:apply-templates select="xhtml:ul/xhtml:li"/>
	</topic>
  </xsl:template>

  </xsl:stylesheet>
