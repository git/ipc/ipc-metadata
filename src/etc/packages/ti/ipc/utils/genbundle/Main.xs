/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 *  ======== Main.xs ========
 */
function main(args)
{
    if (args.length < 3) {
        print("Usage: ti.utils.genbundle outDir bundleName templateDir");
        return -1;
    }

    var mkpkg = xdc.module("xdc.tools.mkpkg.Main");

    /* TODO: H  Need to do better parameter validation here */
    var bundleDir = args[0];
    var bundleName = args[1];
    var templateDir = args[2];

    var myArgs = new Object();

    /*
     * Coming in, cmd line is name=value pairs.  This creates a var, myArgs,
     * and adds named indexes off it named "name" with value "value".  E.g., if
     * foo=bar is passed on the cmd line, this for loop sets myArgs[foo] = bar.
     * Inside the mkpkg.xdt script, it can reference this via
     * this.arguments[1]['foo'];
     */
    for (var i = 3; i < args.length; i++) {
//        print("Found var:  " + args[i]);
        if (args[i].match("=")) {
            /* there's an '=' char, implying name=value pair */
//            print("Found =");
            var nameValuePair = args[i].split('=');

            /* any further validation required? */
            myArgs[nameValuePair[0]] = nameValuePair[1];
        }
    }

    mkpkg.mkpkg(templateDir, bundleDir, bundleName, myArgs);
}
