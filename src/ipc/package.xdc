/*
 * Copyright (c) 2012-2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
requires ti.ipc;
requires ti.ipc.mm;
requires ti.sdo.ipc;
requires ti.sdo.ipc.gates;
requires ti.sdo.ipc.heaps;
requires ti.sdo.ipc.interfaces;
requires ti.sdo.ipc.notifyDrivers;
requires ti.sdo.ipc.nsremote;
requires ti.sdo.ipc.transports;

requires ti.sdo.ipc.family;
requires ti.sdo.ipc.family.arctic;
requires ti.sdo.ipc.family.omap4430;
requires ti.sdo.ipc.family.c647x;
requires ti.sdo.ipc.family.ti81xx;
requires ti.sdo.ipc.family.dm6446;
requires ti.sdo.ipc.family.da830;
requires ti.sdo.ipc.family.omap3530;
requires ti.sdo.ipc.family.c6a8149;
requires ti.sdo.ipc.family.vayu;
requires ti.sdo.ipc.family.am65xx;
requires ti.sdo.ipc.family.tci663x;
requires ti.sdo.ipc.family.tda3xx;
requires ti.sdo.ipc.productview;
requires ti.sdo.utils;

requires ti.ipc.family.omap54xx;
requires ti.ipc.family.omapl138;
requires ti.ipc.family.tci6614;
requires ti.ipc.family.tci6638;
requires ti.ipc.family.vayu;
requires ti.ipc.family.am65xx;
requires ti.ipc.namesrv;
requires ti.ipc.ipcmgr;
requires ti.ipc.rpmsg;
requires ti.ipc.tests;
requires ti.ipc.transports;
requires ti.ipc.remoteproc;

/* to rename */
requires ti.grcm;
requires ti.pm;
requires ti.srvmgr;
requires ti.srvmgr.omx;
requires ti.srvmgr.omaprpc;
requires ti.trace;
requires ti.deh;


/*!
 *  ======== ipc ========
 *  @_nodoc
 */
package ipc {
}
