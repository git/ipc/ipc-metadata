#
#  ======== makefile ========
#
export XDC_INSTALL_DIR

export IPCTOOLS=$(CURDIR)/src/etc

DOXYGENVER=1.8.9.1

USERGUIDE_VER	= sprugo6e.pdf

GA_SUFFIX   =
ENG_SUFFIX  = _eng

RELEASE_TYPE_SUFFIX     = $($(RELEASE_TYPE)_SUFFIX)

VERSTRDOTSUFF := $(shell cat version.txt)$(RELEASE_TYPE_SUFFIX)
MAJVER := $(shell cat version.txt)$(RELEASE_TYPE_SUFFIX)
MAJMIN := ipc_$(shell cat version.txt | sed -e 's/\./_/g')$(RELEASE_TYPE_SUFFIX)
RELNOTE := ipc_$(shell cat version.txt | sed -e 's/\./_/g')$(RELEASE_TYPE_SUFFIX)

.all-files: j = 1
.all-files: opt = -k
.all-files: t = .interfaces
.all-files: .product .docs .eclipse .zip-file

.interfaces: .productview 
	@echo "running xdc [$@] ..."
	@$(XDC) .interfaces -j $(j) -PR src/ti/sdo

.exports-ipc: $(IPCTOOLS)/packages .productview
	$(XDC_INSTALL_DIR)/xdc XDCPATH="$(IPC_INSTALL_DIR)/packages;$(IPCTOOLS)/packages;$(BIOS_INSTALL_DIR)/packages" release -j $(j) -P src/ipc

.exports: .exports-ipc

PLUGINVERCMD = \
          sed -e 's/version\s*=\s*.*;/version = \"$(VERSTRDOTSUFF)\";/' \
              -e 's/\(ipc_[a-z0-9_]*_release_notes\)/$(RELNOTE)_release_notes/'

.eclipse: .eclipse-only

.eclipse-only: .docs
	@echo creating product plugin
	@src/scripts/cdoc2toc.ksh exports/$(MAJMIN)/docs/cdoc/toc.xml > exports/$(MAJMIN)/docs/cdoc/eclipse_cdoc_toc.xml
	@$(PLUGINVERCMD) src/eclipse/Product.xdc > src/eclipse/Product.xdc.tmp
	mv src/eclipse/Product.xdc.tmp src/eclipse/Product.xdc

	$(XDC_INSTALL_DIR)/xdc -P src/eclipse
	$(XDC_INSTALL_DIR)/xs --xdcpath="$(CURDIR)/exports/$(MAJMIN)/packages;$(BIOS_INSTALL_DIR)/packages;./src;$(XDC_INSTALL_DIR)/packages" \
		xdc.tools.product.plugingen -p exports/$(MAJMIN) -m eclipse.Product

.productview: $(IPCTOOLS)/packages
	@$(XDC_INSTALL_DIR)/xdc XDCPATH="$(IPCTOOLS)/packages;$(BIOS_INSTALL_DIR)/packages" release -j $(j) -P src/ti/sdo/ipc/productview
#	@cp -R src/ti/sdo/ipc/productview $(IPC_INSTALL_DIR)/packages/ti/sdo/ipc

.product: .productview $(IPCTOOLS)/packages/ti/ipc/utils/genbundle/package .exports .gen-release-notes

	@rm -rf exports/$(MAJMIN)
	@mkdir -p exports/$(MAJMIN)

	@echo "making bundle ..."
	@$(XDC_INSTALL_DIR)/xs --xdcpath "$(IPCTOOLS)/packages;src;$(IPC_INSTALL_DIR)/packages" \
		ti.ipc.utils.genbundle \
		exports/$(MAJMIN) \
		$(MAJMIN) \
		product.xdt \
		xdcRootDir=$(XDC_INSTALL_DIR) \
		biosRootDir=$(BIOS_INSTALL_DIR) \
		releaseType=$(RELEASE_TYPE) \
		linuxToolchain=$(GCARMV5T_LINAROVER) \
		qnxVer=$(QNX_VER_DESC) \
                bspVerDRA74x=$(QNX_BSP_VER_DRA74X) \
                bspVerDRA72x=$(QNX_BSP_VER_DRA72X)

	@mkdir -p exports/$(MAJMIN)/packages
	@mkdir -p exports/$(MAJMIN)/linux
	@mkdir -p exports/$(MAJMIN)/qnx
	@mkdir -p exports/$(MAJMIN)/hlos_common
	mkdir -p exports/$(MAJMIN)/docs
	mkdir -p exports/$(MAJMIN)/docs/icons
	mkdir -p exports/$(MAJMIN)/docs/relnotes_archive

	@echo "copying files to product ..."
	@cp -R src/ipc/packages/ti exports/$(MAJMIN)/packages/ti
# Copying product build file
	@cp $(IPC_INSTALL_DIR)/ipc-bios.bld exports/$(MAJMIN)/ipc-bios.bld
	@cp $(IPC_INSTALL_DIR)/*.mak exports/$(MAJMIN)
	@cp -R $(IPC_INSTALL_DIR)/hlos_common/* exports/$(MAJMIN)/hlos_common
# TODO: Blindly copying the linux built tree into the product tree and all the
#       nessessary autotoolsfiles.  Need Makefile and object file cleanup
	@cp -R $(IPC_INSTALL_DIR)/linux/* exports/$(MAJMIN)/linux
	@cp $(IPC_INSTALL_DIR)/aclocal.m4 exports/$(MAJMIN)
	@cp $(IPC_INSTALL_DIR)/configure* exports/$(MAJMIN)
	@cp $(IPC_INSTALL_DIR)/Makefile.* exports/$(MAJMIN)
	@cp -R $(IPC_INSTALL_DIR)/qnx/* exports/$(MAJMIN)/qnx
	@cp $(IPC_INSTALL_DIR)/Android.mk exports/$(MAJMIN)

	@echo "copying docs to product ..."
	@cp src/docs/IPC_Install_Guide_*.pdf exports/$(MAJMIN)/docs
	@cp src/docs/$(USERGUIDE_VER) exports/$(MAJMIN)/docs/IPC_Users_Guide.pdf
	@cp src/docs/tilogo.gif exports/$(MAJMIN)/docs/tilogo.gif
	@cp src/docs/titagline.gif exports/$(MAJMIN)/docs/titagline.gif
	@cp -R src/docs/icons exports/$(MAJMIN)/docs
	@cp -R src/docs/relnotes_archive exports/$(MAJMIN)/docs
	@cp -R src/docs/ipc_release_notes.html exports/$(MAJMIN)/$(MAJMIN)_release_notes.html
	@cp -R src/docs/ipc_reports.html exports/$(MAJMIN)/$(MAJMIN)_reports.html
	@cp src/docs/misra.txt exports/$(MAJMIN)/docs
	@cp src/docs/coverity.txt exports/$(MAJMIN)/docs

.zip-file: .product .docs .eclipse
	@echo "archive product to a .zip file ..."
	@cd exports; zip -r $(MAJMIN) $(MAJMIN)

#
#  ======== .doxygen ========
#  build doxygen from headers in ti/ipc
#
.doxygen: .product $(IPCTOOLS)/doxygen_templates/$(DOXYGENVER) 
	@echo "Generating doxygen ............"
	cd $(IPCTOOLS)/default; ln -s -f ../doxygen_templates/$(DOXYGENVER) doxygen_templates
	$(IPCTOOLS)/tdox "exports/$(MAJMIN)/packages/ti/ipc exports/$(MAJMIN)/packages/ti/grcm exports/$(MAJMIN)/packages/ti/pm src/etc_doxygen" \
		exports/$(MAJMIN)/docs/doxygen \
		-s "exports/$(MAJMIN)/packages/" \
		-e "package */ti/ipc/namesrv/* */ti/ipc/remoteproc/* */ti/ipc/rpmsg/* */ti/ipc/transports/* */ti/ipc/*/*/*" \
		-n "IPC API" \
		-d \
		-v $(MAJVER) \
		-o src/etc_doxygen/doxyoverride \
		-f "*.h doxygen.txt"
	@chmod -R u+w exports/$(MAJMIN)/docs/doxygen
	@chmod -R a+r exports/$(MAJMIN)/docs/doxygen
	@rm -rf exports/$(MAJMIN)/docs/doxygen/latex

#
#  ======== .docs ========
#  build cdoc information
#
.docs: .doxygen 
	@echo "Generating cdoc ..."
	# filter out undesired packages with xdcpkg and egrep
	@$(XDC_INSTALL_DIR)/xs  --xdcpath "$(CURDIR)/src/ipc/packages;$(BIOS_INSTALL_DIR)/packages;$(XDC_INSTALL_DIR)/packages" \
	    xdc.tools.cdoc -od:exports/$(MAJMIN)/docs/cdoc -s -P -title "IPC API Documentation" ` \
	    $(XDC_INSTALL_DIR)/bin/xdcpkg src/ipc/packages | \
	    egrep -v -e "/tests*|/stress|/manual|/samples|/examples|/apps" | \
	    xargs`

#
# ======= Release Notes
.gen-release-notes:
	#Update relnotes archive with html file to display directory
	echo "Making index.html file for release notes archive directory..."
	@cd src/docs/relnotes_archive/; ../../scripts/GenRelArchive.sh > index.html

$(IPCTOOLS)/packages/ti/ipc/utils/genbundle/package:
	$(XDC_INSTALL_DIR)/xdc XDCPATH="$(XDC_INSTALL_DIR)/packages;$(IPCTOOLS)/packages" release -Pr $(IPCTOOLS)/packages

#
#  ======== .clean ========
#
#  clean rules
#
.clean: .clean-product
	@echo cleaning exports ...
	rm -rf exports
	mkdir exports

.clean-product:
	@rm -rf exports/$(MAJMIN)


.clean-tests:
	@echo "Running xdc clean"
	@$(XDC) clean -P \
            `$(XDCROOT)/bin/xdcpkg src/ti/sdo | \
            egrep -e "/tests|/manual" | xargs`

